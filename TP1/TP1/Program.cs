﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entrez votre Poids");
            double poids = double.Parse(Console.ReadLine());
            Console.WriteLine("Entrez votre Taille");
            double taille = double.Parse(Console.ReadLine());
            double imc = Math.Round(poids / Math.Pow(taille, 2),2); // Calcule de l'imc: poids / Taille² arrondie aux dixièmes près.
            if (imc <= 16.5)
            {
                Console.WriteLine("votre IMC est de " + imc + ", vous est en Dénutrition");
            }
            else if (imc > 11.6 && imc <= 18.5)     // Si l'IMC est inférieur à 11.6, je considere qu'il y'a une erreure
            {
                Console.WriteLine("votre IMC est de " + imc + ", vous est en Maigreur");
            }
            else if (imc > 18.5 && imc <= 25)
            {
                Console.WriteLine("votre IMC est de " + imc + ", vous est en Corpulence normal");
            }
            else if (imc > 25.1 && imc <= 30)
            {
                Console.WriteLine("votre IMC est de " + imc + ", vous est en Surpoids");
            }
            else if (imc > 30.1 && imc <= 35)
            {
                Console.WriteLine("votre IMC est de " + imc + ", vous est en Obésité modérée");
            }
            else if (imc > 35.1 && imc <= 40)
            {
                Console.WriteLine("votre IMC est de " + imc + ", vous est en Obésité sévère");
            }
            else if (imc > 40 && imc <=50)      // Si l'IMC est supérieur à 50 , je considere qu'il y'a une erreure
            {
                Console.WriteLine("votre IMC est de " + imc + ", vous est en Obésité morbide ou massive");
            }
            else
            {
                Console.WriteLine("PROBLEME !, votre IMC est de " + imc + ", il y'a sûrement un problème");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetCursorPosition(22, 1);
            Console.WriteLine("Devinez un nombre entre 1 et 50");
            int n = 0;
            Random rand = new Random();     //Crée une variable aléatoire
            int r = rand.Next(50);          //Incrémente cette variable r avec une nombre génèrer aléatoirement dans une plage de nombre entre 0 et 50 
            int i=0;
            while (n != r)                  // une boucle Tant que qui à comme condition (tant que la varialbe n est différente de r)
            {
                n = int.Parse(Console.ReadLine());
                i++;
              if (n == r)
                {
                    Console.SetCursorPosition(22, 1);
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine("Bravo !, vous avez réussie après "+i+" essais.");
                }
                else if (n > 0 && n <= r)
                {
                    Console.Clear();
                    Console.WriteLine("Devinez un nombre entre 1 et 50");
                    Console.SetCursorPosition(22, 1);
                    Console.WriteLine("Dommage!, c'est plus haut");    
                }
                else if (n >= r && n <51)
                {
                    Console.Clear();
                    Console.WriteLine("Devinez un nombre entre 1 et 50");
                    Console.SetCursorPosition(22, 1);
                    Console.WriteLine("Dommage!, c'est plus bas"); 
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Devinez un nombre entre 1 et 50");
                    Console.SetCursorPosition(22, 1);
                    Console.WriteLine("Votre nombres n'est pas comprit entre 1 et 50, recommencé"); 
                }
            }

        }


    }
}